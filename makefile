# Copyright (C) 2014-2015  Nicola Spanti <dev@nicola-spanti.info>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


SRC_DIR=src
BIN_DIR=bin
DOC_DIR=doc

CC=gcc
INCLUDES=-I./$(SRC_DIR)
DEBUG_FLAGS=-O0 -g
CFLAGS=-std=c99 -Wall -Wextra -Wpedantic -pedantic-errors -Werror \
	$(INCLUDES) -O2 $(DEBUG_FLAGS) \
	-D_FORTIFY_SOURCE=2 -fstack-protector-all -fPIC \
	-Wl,-z,relro -Wl,-z,now -Wno-unused-command-line-argument
RM=rm -f

PACKAGE=halma-game
FILES_TO_ARCHIVE=$(SRC_DIR)/ makefile doxygen_configuration.ini \
	LICENSE* README* \
	.gitignore .editorconfig .dir-locals.el .gitlab-ci.yml


.PHONY: \
	$(DOC_DIR) clean \
	archives default-archive zip tar-gz tar-bz2 tar-xz 7z


all: tests doc default-archive


tests: bin
	cppcheck --verbose --quiet --error-exitcode=1 \
		--enable=warning,style,performance,portability $(SRC_DIR)


bin: $(BIN_DIR)/$(PACKAGE)-text $(BIN_DIR)/libtab2dchar.a $(BIN_DIR)/libtab2dchar.so

$(BIN_DIR)/$(PACKAGE)-text: \
		$(BIN_DIR)/stdio_functions.o       \
		$(BIN_DIR)/string_functions.o      \
		$(BIN_DIR)/tab_2d_char_essential.o \
		$(BIN_DIR)/tab_2d_char_print.o     \
		$(BIN_DIR)/tab_2d_char_scan.o      \
		$(BIN_DIR)/tab_2d_char_file.o      \
		$(BIN_DIR)/halma_game_essential.o  \
		$(BIN_DIR)/tab_2d_char_fill_zone.o \
		$(BIN_DIR)/halma_game_player.o     \
		$(BIN_DIR)/halma_game_players.o    \
		$(BIN_DIR)/halma_game_board.o      \
		$(BIN_DIR)/main-c.o
	$(CC) $(CFLAGS) -fPIE -pie \
		$(BIN_DIR)/stdio_functions.o       \
		$(BIN_DIR)/string_functions.o      \
		$(BIN_DIR)/tab_2d_char_essential.o \
		$(BIN_DIR)/tab_2d_char_print.o     \
		$(BIN_DIR)/tab_2d_char_scan.o      \
		$(BIN_DIR)/tab_2d_char_file.o      \
		$(BIN_DIR)/halma_game_essential.o  \
		$(BIN_DIR)/tab_2d_char_fill_zone.o \
		$(BIN_DIR)/halma_game_player.o     \
		$(BIN_DIR)/halma_game_players.o    \
		$(BIN_DIR)/halma_game_board.o      \
		$(BIN_DIR)/main-c.o                \
		-o $(BIN_DIR)/$(PACKAGE)-text

$(BIN_DIR)/main-c.o: $(SRC_DIR)/stdio_functions.h $(SRC_DIR)/tab_2d_char_io.h  $(SRC_DIR)/tab_2d_char_file.h $(SRC_DIR)/halma_game_essential.h $(SRC_DIR)/main-c.c
	$(CC) $(CFLAGS) \
		-c $(SRC_DIR)/main-c.c \
		-o $(BIN_DIR)/main-c.o

$(BIN_DIR)/halma_game_board.o: $(SRC_DIR)/tab_2d_char_fill_zone.h $(SRC_DIR)/halma_game_players.h $(SRC_DIR)/halma_game_board.h $(SRC_DIR)/halma_game_board.c
	$(CC) $(CFLAGS) \
		-c $(SRC_DIR)/halma_game_board.c \
		-o $(BIN_DIR)/halma_game_board.o

$(BIN_DIR)/halma_game_players.o: $(SRC_DIR)/halma_game_player.h $(SRC_DIR)/halma_game_players.h $(SRC_DIR)/halma_game_players.c
	$(CC) $(CFLAGS) \
		-c $(SRC_DIR)/halma_game_players.c \
		-o $(BIN_DIR)/halma_game_players.o

$(BIN_DIR)/halma_game_player.o: $(SRC_DIR)/bool.h $(SRC_DIR)/string_functions.h $(SRC_DIR)/halma_game_player.h $(SRC_DIR)/halma_game_player.c
	$(CC) $(CFLAGS) \
		-c $(SRC_DIR)/halma_game_player.c \
		-o $(BIN_DIR)/halma_game_player.o

$(BIN_DIR)/halma_game_essential.o: $(SRC_DIR)/tab_2d_char_print.h $(SRC_DIR)/halma_game_essential.h $(SRC_DIR)/halma_game_essential.c
	$(CC) $(CFLAGS) \
		-c $(SRC_DIR)/halma_game_essential.c \
		-o $(BIN_DIR)/halma_game_essential.o

$(BIN_DIR)/libtab2dchar.so: $(BIN_DIR)/stdio_functions.o $(BIN_DIR)/tab_2d_char_essential.o $(BIN_DIR)/tab_2d_char_print.o $(BIN_DIR)/tab_2d_char_scan.o $(BIN_DIR)/tab_2d_char_file.o
	$(CC) $(CFLAGS) \
		-o $(BIN_DIR)/libtab2dchar.so -shared \
		$(BIN_DIR)/tab_2d_char_*.o

$(BIN_DIR)/libtab2dchar.a: $(BIN_DIR)/stdio_functions.o $(BIN_DIR)/tab_2d_char_essential.o $(BIN_DIR)/tab_2d_char_print.o $(BIN_DIR)/tab_2d_char_scan.o $(BIN_DIR)/tab_2d_char_file.o
	ar -rv $(BIN_DIR)/libtab2dchar.a $(BIN_DIR)/tab_2d_char_*.o

$(BIN_DIR)/tab_2d_char_file.o: $(SRC_DIR)/bool.h $(SRC_DIR)/tab_2d_char_print.h $(SRC_DIR)/tab_2d_char_file.h $(SRC_DIR)/tab_2d_char_file.c
	$(CC) $(CFLAGS) \
		-c $(SRC_DIR)/tab_2d_char_file.c \
		-o $(BIN_DIR)/tab_2d_char_file.o

$(BIN_DIR)/tab_2d_char_scan.o: $(SRC_DIR)/stdio_functions.h $(SRC_DIR)/tab_2d_char_essential.h $(SRC_DIR)/tab_2d_char_scan.h $(SRC_DIR)/tab_2d_char_scan.c
	$(CC) $(CFLAGS) \
		-c $(SRC_DIR)/tab_2d_char_scan.c \
		-o $(BIN_DIR)/tab_2d_char_scan.o

$(BIN_DIR)/tab_2d_char_print.o: $(SRC_DIR)/tab_2d_char_essential.h $(SRC_DIR)/tab_2d_char_print.h $(SRC_DIR)/tab_2d_char_print.c
	$(CC) $(CFLAGS) \
		-c $(SRC_DIR)/tab_2d_char_print.c \
		-o $(BIN_DIR)/tab_2d_char_print.o

$(BIN_DIR)/tab_2d_char_fill_zone.o: $(SRC_DIR)/tab_2d_char_essential.h $(SRC_DIR)/tab_2d_char_fill_zone.h $(SRC_DIR)/tab_2d_char_fill_zone.c
	@mkdir -p $(BIN_DIR)/
	$(CC) $(CFLAGS) \
		-c $(SRC_DIR)/tab_2d_char_fill_zone.c \
		-o $(BIN_DIR)/tab_2d_char_fill_zone.o

$(BIN_DIR)/tab_2d_char_essential.o: $(SRC_DIR)/bool.h $(SRC_DIR)/tab_2d_generic_static.h $(SRC_DIR)/tab_2d_char_essential.h $(SRC_DIR)/tab_2d_char_essential.c
	@mkdir -p $(BIN_DIR)/
	$(CC) $(CFLAGS) \
		-c $(SRC_DIR)/tab_2d_char_essential.c \
		-o $(BIN_DIR)/tab_2d_char_essential.o

$(BIN_DIR)/position_2d_uint_pair.o: $(SRC_DIR)/position_2d_uint.h $(SRC_DIR)/position_2d_uint_pair.h $(SRC_DIR)/position_2d_uint_pair.c
	@mkdir -p $(BIN_DIR)/
	$(CC) $(CFLAGS) \
		-c $(SRC_DIR)/position_2d_uint_pair.c \
		-o $(BIN_DIR)/position_2d_uint_pair.o

$(BIN_DIR)/position_2d_uint.o: $(SRC_DIR)/position_2d_uint.h $(SRC_DIR)/position_2d_uint.c
	@mkdir -p $(BIN_DIR)/
	$(CC) $(CFLAGS) \
		-c $(SRC_DIR)/position_2d_uint.c \
		-o $(BIN_DIR)/position_2d_uint.o

$(BIN_DIR)/string_functions.o: $(SRC_DIR)/string_functions.h $(SRC_DIR)/string_functions.c
	@mkdir -p $(BIN_DIR)/
	$(CC) $(CFLAGS) \
		-c $(SRC_DIR)/string_functions.c \
		-o $(BIN_DIR)/string_functions.o

$(BIN_DIR)/stdio_functions.o: $(SRC_DIR)/stdio_functions.h $(SRC_DIR)/stdio_functions.c
	@mkdir -p $(BIN_DIR)/
	$(CC) $(CFLAGS) \
		-c $(SRC_DIR)/stdio_functions.c \
		-o $(BIN_DIR)/stdio_functions.o


$(DOC_DIR): $(SRC_DIR)/*.h etc/doxygen_configuration.ini
	doxygen etc/doxygen_configuration.ini
	@mkdir -p $(BIN_DIR)/
	# PDF with LaTeX
	cd $(DOC_DIR)/latex/ && make


archives: zip tar-gz tar-bz2 tar-xz 7z

default-archive: tar-xz

zip: $(PACKAGE).zip

$(PACKAGE).zip: $(FILES_TO_ARCHIVE)
	zip $(PACKAGE).zip -r -- $(FILES_TO_ARCHIVE)

tar-gz: $(PACKAGE).tar.gz

$(PACKAGE).tar.gz: $(FILES_TO_ARCHIVE)
	tar -zcvf $(PACKAGE).tar.gz -- $(FILES_TO_ARCHIVE)

tar-bz2: $(PACKAGE).tar.bz2

$(PACKAGE).tar.bz2: $(FILES_TO_ARCHIVE)
	tar -jcvf $(PACKAGE).tar.bz2 -- $(FILES_TO_ARCHIVE)

tar-xz: $(PACKAGE).tar.xz

$(PACKAGE).tar.xz: $(FILES_TO_ARCHIVE)
	tar -cJvf $(PACKAGE).tar.xz -- $(FILES_TO_ARCHIVE)

7z: $(PACKAGE).7z

$(PACKAGE).7z: $(FILES_TO_ARCHIVE)
	7z a -t7z $(PACKAGE).7z $(FILES_TO_ARCHIVE)


clean: clean-bin clean-profiling clean-tmp clean-doc clean-tests-files clean-archives

clean-bin:
	$(RM) -rf -- \
		$(OBJ_DIR) $(BIN_DIR) \
		*.o *.a *.so *.ko *.lo *.dll *.out

clean-profiling:
	$(RM) -rf -- callgrind.out.*

clean-tmp:
	$(RM) -r -- \
		*~ *.bak *.backup .\#* \#* \
		*.sav *.save *.autosav *.autosave \
		$(SRC_DIR)/*~ $(SRC_DIR).\#* $(SRC_DIR)/\#* \
		$(SRC_DIR)/*.bak $(SRC_DIR)/*.backup \
		*.log *.log.* error_log* \
		.cache/ .thumbnails/

clean-doc:
	$(RM) -r -- doc documentation

clean-tests-files:
	$(RM) -rf -- \
		a b c a.* b.* c.* \
		test.c tests.c \
		test.cc tests.cc test.cpp tests.cpp \
		test.java tests.java \
		test.py tests.py \
		junit* report*

clean-archives:
	$(RM) -- \
		*.zip *.tar.* *.tgz *.7z *.gz *.bz2 *.lz *.lzma *.xz \
		*.deb *.rpm *.exe *.msi *.dmg *.jar *.apk *.ipa *.iso

clean-git:
	git clean -fdx
