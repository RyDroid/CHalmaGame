# License

## Source code

If a license is specified in a file, the file is of course under this license.
If there is an error due to copy-paste or copyleft, it must be reported.

If it is not specified, source code of [Nicola Spanti](https://www.nicola-spanti.info/fr/) is under [GNU Lesser General Public License](https://www.gnu.org/licenses/lgpl.html), as published by the [Free Software Foundation](https://fsf.org/), version 3 or at your option any later.
Liedri Hichan has not yet defined his license policy.
Hopefully, he will choose free/libre licence(s).
New contributors have to explicitly publish their contributions under LGPLv3+, GPLv3+, AGPLv3+, or any compatible license with the 3 already quoted.
